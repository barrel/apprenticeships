# Welcome to Your Apprenticeship

We're excited to have you here. This packet contains information that past
apprentices have found helpful.

We recommend looking through it now and referring back to it later. Think of
this packet as your Hitchhiker's Guide during your apprenticeship. Don't Panic!

This packet is available at [https://gitlab.com/barrel/apprenticeship].
Feel free to file a pull request to improve any aspect of it, no matter how tiny.

# O-Week

Your first week will be spent learning a bit about the agency ropes of Barrel: 

**Day 1: Introduction to Barrel**
- Before the meeting
  - Office tour & introductions 
  - Office setup (computers, seating, etc.)
- Meeting Objectives
  - Introductions 
  - Overview of apprenticeship
  - What will this week look like?
  - What will this apprenticeship look like?
    - Mentor/ Mentee relationship
    - Focus on project work
    - Meeting cadence (Daily and Weekly Check-ins)
    - Daily Homework and self learning
  - Things to think about for the rest of the week
    - What are your goals?
    - What are the technologies that you’re interested in learning?
  - Questions
- Follow-Up Tasks
  - Meet the dev team
  - Housekeeping tasks (Trinet enrollment, etc)
  - Prepare your development environment
    - IDE of choice
    - Node, Brew, NPM, Lando, Xcode
    - Git, Git Flow, SourceTree
    - Sketch (ask mentor for license)
    - Transmit (license in 1password)
    - Sequel Pro
    - JsonView (Chrome extension), Postman, Recordit
  - Familiarization with the Knowledge Base (kb.barrelny.com)
  - Familiarization with [Best Practices](https://gitlab.com/barrel/barrel-dev-best-practices)
  - Work through [Project 1: Base Themes](projects/01-base-themes.md)

**Day 2: Introduction to Development**
- Meeting Objectives
  - Debrief on yesterday
  - Introduction to today
  - Mentors to talk about the development process for each of their active projects
    - Each mentor walks through 3 projects and explains the stack, development workflow (how, who, when) 
- Follow-Up Tasks
  - Work through [Project 2: Docker & Vagrant](projects/02-docker-vagrant-vm-native.md)
  - Work through [Project 3: Dotfiles](projects/03-dotfiles.md)
  - Work through [Project 4: Dependencies & Task Runners](projects/04-deps-taskrunner-build.md)


**Day 3: Introduction to Process**
- Meeting Objectives *1.5 hour meeting
  - Debrief from yesterday's projects
  - Roles and responsibilities at Barrel (inter-team)
  - Introduction to JIRA
  - The anatomy of a JIRA ticket
- Follow-Up Tasks
  - Work through [Project 5: Git flow](projects/05-git-workflows-processes.md)
  - Work through [Project 6: Functional Specs](projects/06-functional-specs.md)


**Day 4: Deeper dive into to Platform (Shopify or Wordpress)**  
- Meeting Objectives
  - Debrief on yesterday's projects
  - Outline the history, role and status of base themes
- Follow-Up Tasks
  - Become familiar with the base-themes
  - Work through [Project 7: Wordpress Base-Theme](projects/07-wordpress-base-theme.md)
  - Work through [Project 8: Shopify Base-Theme](projects/08-shopify-base-theme.md)

**Day 5: Goal selection and next steps**
- Meeting Objectives
  - *This meeting will be held later in the day
  - Formalize 1 - 3 goals to address during the apprenticeship
  - Walkthrough the next week's agenda
- Daily Tasks (before the meeting)
  - Continue work on yesterday's projects
  - Talk to your mentor about preparing for next weeks's projects

**From there..**

We'll take a dive into project work and grow your skills together. There will be a check-in every day with your mentor to check-in on your immediate tasks and a check-in very week to align on your goals and progression.

**Month Three**

In your third month, you'll meet with the Director of Technology to talk about your future at Barrel. Over the course of the first two months there will be plenty of talk about strengths and weaknesses. These will be the focus on this meeting.

## How are we doing?

You will meet with your mentor once a week on Friday to talk through your
progress.

You are encouraged to create a list of the goals you have for your
apprenticeship on an overall basis as well as to maintain a weekly list of
smaller, specific goals. You should work with your mentor to set these and track
your progress against them.

When you switch mentors, you will have a Mentor Hand-off meeting. This is a 
meeting with both mentors to talk through the previous month and the plan for
the next month.

## Pairing during the week

Pairing is the fastest and most effective way to learn, so it should be a
priority for you and your mentor. However, do not expect to pair 100% of the
time. One particularly effective method of pairing is Ping-Ponging. The idea is
for Person 1 to write a test, Person 2 writes the code to make the test pass.
Then Person 2 writes the next test and Person 1 writes the code to make the test
pass. Rinse, repeat.

## Self-guided learning

Whenever appropriate or applicable to your work, you should take time away from
client work to do some self-guided learning. This can include reading material
from books, tutorials, or learning about and trying out a technique or library.

For reading material, we recommend starting with Apprenticeship Patterns. We 
also have a library of [best practices](https://gitlab.com/barrel/barrel-dev-best-practices/) and a knowledgebase.

# Getting the Most Out of Your Apprenticeship

## Expectations

Your apprenticeship is an opportunity. The more you put into it, the more you
will get out of it.

There is some structure to guide you through the program and also people
available to help you along the way, but the engine of this experience is your
effort. Learning should take up some of your evening/weekend time.

You should expect this to be a mentally intense period of your life where you
are learning a great deal. Hard work pays off.

## Breakable Toy

A [Breakable Toy](http://redsquirrel.com/dave/work/a2j/patterns/BreakableToys.html)
is a side project you are excited and passionate about.

This project should be an anchor to your learning, allowing you to raise new
questions and a place to apply new skills/techniques. To get the most out of it,
you should put the code in GitHub and give your mentors and apprentice cohort
(if applicable) access to the project. You should also deploy it publically.

Don't be afraid to ask for help!

## Feedback

It's not easy, but you should get comfortable asking for and receiving feedback.
People are nice and none of it is personal. It's a great way to develop
professionally.

# Tools

## Laptop setup

Your mentor will help you get your laptop set up. This will include setting up
a [bash profile](https://scriptingosx.com/2017/04/about-bash_profile-and-bashrc-on-macos/),
choosing an IDE with `.editorconfig` [support](http://editorconfig.org/#download).
You will see that editorconfig is a method to keep consistency in coding style
between contributors of a project. While we use Macs and others Windows, the 
internet world is grounded in [unix and linux](https://www.dwheeler.com/secure-programs/Secure-Programs-HOWTO/history.html). As a software developer, understanding the 
operating system is almost as important as the software you developing it under.
Becoming familiar with the tools on your computer will assist you in your journey.
You will primarily interface with a number of these tools over the command line
via the [terminal emulator](https://en.wikipedia.org/wiki/Terminal_emulator), one
of the most important tools itself. Two additional applications of great use to
developers are the manuals, also called "man" in unix/linux parlance, and the 
[package managers](https://en.wikipedia.org/wiki/List_of_software_package_management_systems).

### Dotfiles

Dotfiles are the files in your home directory (referred to as "~") that start
with a dot, like "~/.bashrc". They are used to configure various programs, e.g.
"~/.vimrc" sets some global options for Vim. Take a look at [this dotfile repo](https://github.com/wturnerharris/dotfile), 
and feel free to fork it. We also encourage you to begin to setup and collect 
your own set of bash [scripts and aliases](https://gist.github.com/wturnerharris/8b39df6004418f5a4935d99e53d435cc).

### Git

Version control is the system by which all code is tracked. Think of it like
Microsoft Word's track changes but specifically for code. You will learn many
of the operations as well as workflows to contribute to a git project. We use
primarily the [centralized and gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows) workflows.

## Logins

We use many different websites, and everyone should have accounts for them. 
Having access will make it much easier for you to participate in any internal 
conversations. For any account that is to be shared with the team, we use
[1 Password](https://1password.com/).

The list of services we use is below. By the end of your first week, you 
should have access to all of them.

### Google apps

You should have received an email with your email (NAME@barrelny.com) and
email password.

We use the calendar extensively - add someone else's calendar such as your 
mentor's to keep track of what other people are doing. If you need to meet 
about a project, be sure to invite and schedule a meeting using calendar.

### Dropbox

We use dropbox to share large files, design files, fonts, and other static
assets. Never share passwords nor code on dropbox.

### GitHub / GitLab

We use GitLab for all private repositories. GitHub is also a rather well-known
git repository and you should certainly connect your account with ours. Work 
with your mentor if you do not have access.

### Slack

We primarily use HipChat to communicate one-off messages and to facilitate in
completing our daily work. We also use Skype and [Slack](https://barrelny.slack.com/). 

### Basecamp

We use [Basecamp](https://basecamp.com/) for project management communication.
Use it to post progress with your project, with the team, and with clients.

# Community

Part of your apprenticeship is learning to be part of a development community.
This could be a Javascript Meetup Group, the dev team, or a project's team.

## Meeting People

We are a friendly bunch and we'd like to get to know you better! That being
said, it's easy to feel intimidated or uncomfortable in a new environment. Some
things you should try:

* Take a walk break and clear your mind.

* Go get lunch/coffee/breakfast with anyone in the office.

* Suggest a social event for your project team.

### Daily Lunches

We order lunch for the whole office daily! Please let your mentor know if you 
have any allergies or dietary restrictions.

## Who?

If you're having trouble matching names to faces, check out
[https://www.barrelny.com/about], which has pictures and names of everyone at
the company (except apprentices).

# Improve The Apprentice Program

We encourage you to propose changes to the apprentice process, including hiring,
onboarding, and anything else that you think could be better. Everyone at is 
constantly trying to do a better job and we welcome feedback.

As an apprentice, you have much better insight into what could make the process
better for you than does someone who's been working here for five years.

Feedback is always, always appreciated on everything we do. If you have such
feedback on the overall apprentice program, please discuss the issue with your
mentor, or open an issue on this repository.

# Ready to Get Started?

Besides any of the links and side lookups you've taken from this page, have a 
look at the [projects](./projects/) directory in this repo. 

[← Back to Index](../onboarding-packet.md)

# Project: Wordpress Base-Theme
Start a new project and create your first module!

1. Follow [this document](https://docs.google.com/document/d/19W57tD2zPWJstSPmmVvtQuW5JXZE0zhpAZIcHgt5Xi8/edit#heading=h.cng5qknjmfnp)
    to create a new project on your pantheon account and duplicate the `barrel-base` theme. Let's call the new theme `test-theme`.
    (May need some help setting up the initial pantheon project). 
2. Spin up your new site with lando and activate your new theme
3. Create a new page template php file called "Test Page". The file name should be test-page.php.
4. Create a new page in your wordpress dashboard and set the "Page Template" dropdown to the template created in step 3.
5. Create a new module in your `test-theme` that `echo`s a message and include it on the page template's php file. 
    Check out the `create_module` script in the `private/scripts` directory.
6. Add a new ACF Field group called "Test Page Fields". Assign it to only show up on your "Test Page" template. Create a text
    field called "Test Message". 
7. Save the field and return to the dashboard editor for the page you created in step 3. You should see your ACF fields on the screen now. 
    Add a message to the field you created and save the page.
8. Change your test-page.php template file to echo the value of the "Test Message" field you just created. 
    Some documentation on [ACF](https://www.advancedcustomfields.com/resources).
9. Use the test-page template to port over the code from your test project (the knob creek landing page). Be sure to break the template out into 
    modules. Use at least 3 different types of ACF fields. Use the "post" post-type for the grid (and create some dummy posts to loop through). 

Things to consider:
- Keep in mind that the `feature/update-bcli` branch is currently the most stable branch. We'll likely need to release this before starting this project. 
- Documentation is your friend! If you're not sure how to do something, don't be afraid to research and try out some solutions.
- Keep the structure of the landing page as modular as possible. Review other Barrel projects to see how we've coded similar projects in the past.
- When porting over your landing page, consider where content can be intuitively managed. Try and keep hardcoded content to a minimum.
  - Where would the "client" expect to update the hero section?
  - Where would the "client" go to add a new blog post?
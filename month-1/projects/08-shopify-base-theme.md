[← Back to Index](../onboarding-packet.md)

# Project: Shopify Base-Theme
Create a new development theme on the shopify base theme. 

1. Login to the Shopify partner portal and find the store for "https://base-dev.myshopify.com".
2. Click on the login button to enter the store.
2. Clone the existing live theme and rename it with $YOUR_NAME-dev. (If you haven't done this already).
3. Create a new section that outputs a simple message ("hello, world") and include it on the home page. 
4. Update your section to accept a `message` setting. If the setting is not empty, its value should be used
    for the message. Otherwise, the section's message should output "hello, world". 
5. Pull the code from your test project into this theme and turn the homepage into the landing page you created (Knob Creek landing page). 
    Be sure to break the template into sections. 

Things to consider:
- Documentation is your friend! If you're not sure how to do something, don't be afraid to research and try out some solutions.
- Keep the structure of the landing page as modular as possible. Review other Barrel projects to see how we've coded similar projects in the past.
- When porting over your landing page, consider where content can be intuitively managed. Try and keep hardcoded content to a minimum.
  - Where would the "client" expect to update the hero section?
  - Where would the "client" go to add a new blog post?
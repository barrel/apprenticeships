[← Back to Index](../onboarding-packet.md)

# Project: Dependencies / Taskrunners / Builds

Every project generally has some tooling in place to assist with the repetitive tasks involved with building a website. This includes processes to automate rebuilding styles and scripts, compressing images, and other tasks like copy and upload. In addition, most projects will have to rely on a core set of software packages, or dependencies, to function. In many cases, package managers like npm and composer will be used to manage and track those dependencies for the project.

## Questions to answer through research
- What is webpack and how does it different to Gulp? 
- What are other relevant task runners in the wild? 
- What is a dependency, and what are some considerations for dependency management in a software package? 
- What are some package managers and in what cases will you use them?
- What is an NPM script? What is NPX?

## Execute
- Pull down either the base Shopify theme or the base wordpress theme. Remove the Barrel CLI and get both scripts and styles compiling without the Barrel CLI package.
[← Back to Index](../onboarding-packet.md)

# Project: Refactoring and Port / Migration

Did you have an initial test project? We are going to use that for this next project. We're using the base WordPress and Shopify themes. 

## Research

WordPress and Shopify are two platforms we use the most. If you are not already familiar with these platforms, you should do some investigation into their utility, basic operation, and the documention for how to develop on them. As you execute, continue to find tutorials and materials to supplement your knowledge about the systems, processes, and tools used by the company, your peers, and the world.

## Execute

Use the original test project as the source material. Port over, or migrate, your work to these new systems using the ones provided by the company. As you you work on the project, update the team members involved regularly and as-needed. 

### Project Specifics

Every project has a few basic requirements. 

- Code is almost always hosted on GitLab under the company's private repositories.
- Documentation should always be included and updated as you work. Documentation should concentrate on setting up and getting started for a new developer.
- Never attribute code for client work as your own, but individual libraries or works you create 
- Merge requests are expected for review purposes at regular intervals.
- You may be asked to and you should feel free to discuss your ideas for how to proceed with your work, the reasons behind them, and feedback for improvement.

[← Back to Index](../onboarding-packet.md)

# Project: Git, Workflows, and Processes

Do you just start typing away and writing functions when you start a project? When contributing to a software code repository, do you just commit everything in one go? Even if you do, it's helpful to be prepared with processes and workflows. 

## Research

Compare the different git workflows out in the wild. What are the general components of the code review process?

## Execute

Take on the roles of both executing dev and technical lead for a new feature on your test project. 

1. Add your test project repository to Gitlab
2. Follow our SOPs to add a new modal module (feature branch)
3. Create a new WIP MR for this feature
4. When the modal module is ready for review, review the code through Gitlab
5. Use the MR discussion to post a code improvement
5. Make the improvement in code and push up again 
6. Merge in the feature
7. Package up a new release and merge it into the master branch
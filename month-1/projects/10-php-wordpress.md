[← Back to Index](../onboarding-packet.md)

# Project: PHP and WordPress

## PHP Core Functions and Classes
[PHP Documentation](http://php.net/manual/en/langref.php)

### Highlights
- String functions
  * In php concatenation: the "." works like javascript's "+"
  * esc characters
  ![esc characters](img/esc-char.png)
- Array functions
  * know that: as of PHP 5.4 you can also use the short array syntax, which replaces array() with [].
  * the _key_ in php arrays are optional. If it is not specified, php will use the increment of the largest previously used integer key.
- Variables & Constants
  * defined by define();
- Operators
- stdClass, DomDocument, Date classes
  * stdClass
    * easy way to instantiate an empty generic php object that you can then modify for whatever purpose you choose
  * basically creates a new dom element
  * predefined classes
  * scalar
    * represents 4 of the 10 primitive data types: boolean, integer, float, string
- PHP Syntax: on going
- PHP Tags: on going
- Alternative Syntax for Control Structures
- Namespacing
  * a way of encapsulating items
  * kinda like a pointer to a file
- Errors and Reporting
-- Try-Catch
-- Exceptions
  * getMessage() function is important for Try-Catch
-- Reporting Verbosity

#### Notes
[Things Not To Do](https://kinsta.com/blog/10-things-not-to-do-in-php-7/)
  * #3 Do not use php close at the end of a file
  * **DO NOT** trust use input use filter_var();

## WordPress Core Functions and Classes
[WordPress Documentation](https://codex.wordpress.org/)

### Highlights
- The [Codex](https://codex.wordpress.org/)
- the wp [Loop](https://codex.wordpress.org/The_Loop) Functions (What is the loop?)
  * label $values and $keys when the array is object in the echo statement
  * iterates over an array
- Styles and Scripts (Enqueue functions)
- Action and Filter Hooks
- WordPress Schema and Hierarchy

[Schema](https://codex.wordpress.org/Database_Description)
![wp-erd](img/wp-erd.png)  

[hierarchy](https://developer.wordpress.org/themes/basics/template-hierarchy/)  
![wp hierarchy](img/wp-hierarchy.png)

#### Notes
- Finding and selecting plugins
- Popular
[Plugins](http://www.wpbeginner.com/plugins/20-must-have-wordpress-plugins-for-2015-expert-pick/)  
  * SEO => Yost
  * W3 Total cache/WP Super Cache
  * WooCommerce
  * ACF
  * google analytics  
  * Leadin  
  * [ACF](https://www.advancedcustomfields.com/resources/)

![basic acf functions](img/ACF_basic.png)

[← Back to Index](../onboarding-packet.md)

# Project: Base Themes

- Pull down both the Wordpress and Shopify base themes locally
- Follow each project's README to setup development environments for both
- Make a change to the CSS and confirm that the CSS is recompiling correctly
- Make a change to the JS and confirm that the JS is recompiling correctly
- Hack! Create a new module that does something cool (free reign here)

[← Back to Index](../onboarding-packet.md)

# Project: Functional Specs
Write the functional specifications for a new module for your test project!

1. View this desktop [design](https://invis.io/BRNXZOU9ZAK#/321344037_Landing_Page_-_Desktop_V2)
2. View this mobile [design](https://invis.io/RJO6Y2OF3W9#/321346305_Landing_Page_-_Mobile)
3. Scroll down and view the "Word of Mouth" carousel module in each mock-up
4. Write the functional specs for this module (as if a remote developer was executing) in a single Gitlab ticket.

Things to consider:
- Don't worry about the different typeface and colors
- What dependencies does this module need?
- How will the user interact with this module?
- How does the module change on desktop and mobile?
- Where does the data come from?
- Are there any code examples that might help the developer execute?
- Are there any built out websites that provide examples of what you would like to see in this module?
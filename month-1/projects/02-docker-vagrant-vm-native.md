[← Back to Index](../onboarding-packet.md)

# Project: Docker, Vagrant, Virtual Machine (VM), and Native

Every software development project has a development environment. This environment perfectly emulates your application as it would on the production system. In this case, it's local to your machine and tools for the purposes of rapid feature development. 

## Research

As part of any important task, reasearch is crucial. Investigate the difference between these technologies. Be prepared to explain the relationship between them, and formulate an opinion on which one might be the best for web development.
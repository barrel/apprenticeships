[← Back to Index](../onboarding-packet.md)

# Project: Deployments and Pantheon

## Deployments

When we "deploy" code, or make a release, we are loading the production server with the latest code available. Deployments can include several components such as the codebase, user-uploaded content, and database. When deploying, it's important to know what gets deployed and when. Deployments can and should be automated to a degree.

Imagine you are building a custom deployment script. What sorts of things go in this script? How would you streamline your deployments?

## Pantheon, WP Engine, et al

Both Pantheon and WP Engine allow for deployments with git whereby a git repository like GitHub is used to push code to the live server.

Pantheon, the more recommended of the two, includes command line tools, extra environments that can be automatically created and cloned, and a built-in deployment process. 

By contrast, WP Engine provides only one staging and production environment per install, but it can still deploy via git repository. The database and user-generated content (UGC), or files/downloads, can be managed separately via PHPMyAdmin and S/FTP respectively.
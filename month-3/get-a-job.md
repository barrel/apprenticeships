# Get a Job (your third month)

Our goal is to get you to the point where you can join Barrel as a full
developer.

In month three, you'll meet with your Director and mentor(s) 
to discuss your next steps. During that meeting, you'll be asked "What
do you want to do after your apprenticeship?"

One of our goals is to help you land a developer job at the end of your 
apprenticeship.

If you have decided at that point that this is not for you, no worries. We 
can work with you over the next month to help find job prospects.

If you want to work with us, we will be prepared to either make you an
offer or tell you there isn't a good spot for you at the company.

If you will not be joining us, we will do our best to prepare for and
help you find a job after your apprenticeship is complete.

## Networking

Networking is probably the most important key to success for every single job.
If you network consistently and present yourself as a helpful, honest person,
jobs and opportunities will come flowing to you. At Barrel, we consider networking
to be an important aspect of growth, both personally and professionally. Not only 
will good networking tools equip you to move forward as a professional, they'll 
also enable you in your current space. 

Basics of Networking:

* Do it purposelessly - get to know people when you don't need anything from
  them, then get in touch with them when you do.
* Do it frequently - try to get out and do one thing per week where you meet
  random people.
* Get comfortable telling your story. "So what do you do?" is the #1 question.
  Respond in a positive, short way.
* If you don't have anything to talk about with someone, keep asking the other
  person questions.
* Use "Well I'm going to go get a drink" whenever you don't feel like talking
  anymore to someone.
* Email *everyone* you get a card from to say "Hey" and you're glad you met.
* Give your card out liberally. If you don't have one, get some made.
* Networking is also about helping people. If you know two people who should 
  meet, introduce them.
* Stay in touch. If an article reminds you of someone or you think they would 
  find it interesting, send it to them.
* Set up periodic reminders to prompt you to get in touch with people you like
  and who can be helpful to you. Every 2-3 months is appropriate. Try
  http://followup.cc .
* Most everyone will take a coffee meeting with you, time permitting. Ask them.



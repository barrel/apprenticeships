# Apprentice Onboarding Packet

This is the onboarding packet for new apprentices. It should be given to them on
their first day.

## So You Just Started Mentoring a New Apprentice

Great! Here's how you use this:

* Refer your apprentice to this repository
* Make sure they know that we'd love feedback on this content
* Encourage your apprentice to read through the repo, but it's a reference, not
  a novel. Think of how you'd read a dictionary
* You should read the [mentors/mentor handbook](mentor-handbook.md)

# Credits

Originally sourced from [thoughtbot](https://github.com/thoughtbot/apprenticeship/graphs/contributors)

This repository is maintained by [barrel](https://www.barrelny.com).

# License

This information is © 2018 barrel. It is distributed under the [Creative Commons
Attribution License](http://creativecommons.org/licenses/by/3.0/).
